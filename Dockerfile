FROM ubuntu:bionic

ENV ANSIBLE_HOST_KEY_CHECKING False
ENV TERRAFORM_VERSION 0.11.10

RUN apt-get update && apt-get install software-properties-common python3 python3-pip openssh-client unzip wget -y

RUN apt-add-repository ppa:ansible/ansible
RUN apt-get update && apt-get install ansible -y

RUN pip3 install awscli boto3

RUN wget -O terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"
RUN unzip terraform.zip -d /usr/bin

